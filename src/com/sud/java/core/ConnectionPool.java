package com.sud.java.core;
/*package com.sud.java.automation.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class ConnectionPool {
	 private ConnectionPool() {
	    }
	    
	     * Expects a config in the following format
	     *
	     * poolName = "test pool"
	     * jdbcUrl = ""
	     * maximumPoolSize = 10
	     * minimumIdle = 2
	     * username = ""
	     * password = ""
	     * cachePrepStmts = true
	     * prepStmtCacheSize = 256
	     * prepStmtCacheSqlLimit = 2048
	     * useServerPrepStmts = true
	     *
	     * Let HikariCP bleed out here on purpose
	     
	    public static HikariDataSource getDataSourceFromConfig(
	        Config conf
	        , MetricRegistry metricRegistry
	        , HealthCheckRegistry healthCheckRegistry) {
	        HikariConfig jdbcConfig = new HikariConfig();
	        jdbcConfig.setPoolName(conf.getString("poolName"));
	        jdbcConfig.setMaximumPoolSize(conf.getInt("maximumPoolSize"));
	        jdbcConfig.setMinimumIdle(conf.getInt("minimumIdle"));
	        jdbcConfig.setJdbcUrl(conf.getString("jdbcUrl"));
	        jdbcConfig.setUsername(conf.getString("root"));
	        jdbcConfig.setPassword(conf.getString("root"));
	        jdbcConfig.addDataSourceProperty("cachePrepStmts", conf.getBoolean("cachePrepStmts"));
	        jdbcConfig.addDataSourceProperty("prepStmtCacheSize", conf.getInt("prepStmtCacheSize"));
	        jdbcConfig.addDataSourceProperty("prepStmtCacheSqlLimit", conf.getInt("prepStmtCacheSqlLimit"));
	        jdbcConfig.addDataSourceProperty("useServerPrepStmts", conf.getBoolean("useServerPrepStmts"));
	        // Add HealthCheck
	        jdbcConfig.setHealthCheckRegistry(healthCheckRegistry);
	        // Add Metrics
	        jdbcConfig.setMetricRegistry(metricRegistry);
	        return new HikariDataSource(jdbcConfig);
	    }
}
*/