package com.sud.java.core;

public interface Constants {

	public static enum LocationStatus {
		SUCCESS, NOT_FOUND, FAIL, EXCEPTION, LIMIT_EXCEED
	}
	
	public static final String SP_SELECT_TRIGGERINFO = "{call sp_select_triggerinfo(?)}";
	
	public static final String SP_UPDATE_TRIGGERINFO_CLOSE = "{call sp_update_triggerinfo_close(?,?,?,?,?,?,?,?,?,?,?,?) }";
	
	public static final String SP_UPDATE_TRIGGERINFO = "{call sp_update_triggerinfo(?,?,?,?,?,?,?,?,?,?) }";
	
	public static final String SP_INSERT_TRIGGERINFO = "{call sp_insert_triggerinfo(?,?,?,?,?,?,?,?,?) }";
	
	public static final String SP_UPDATE_ORDERID_IN_TRIGGERINFO = "{call sp_update_orderid_triggerinfo(?,?,?,?,?,?,?,?) }";
	
	public static final String SP_UPDATE_MODI_ORDERID_IN_TRIGGERINFO = "{call sp_update_modifyord_triggerinfo(?,?,?,?,?,?,?,?) }";
	
	
	public static final String SP_SELECT_ORDER_DETAILS = "{call sp_select_orderinfo(?) }";
	
	public static final String SP_INSERT_STOCKLEVEL = "{call sp_insert_stocklevel(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
	
	public static final String SP_SELECT_STOCKLIST = "{call sp_select_stocklist()}";
	
	public static final String SP_SELECT_STOCKLEVEL = "{call sp_select_stocklevel(?)}";

	public static final String SP_SELECT_FILTERED_STOCK = "{call sp_select_filtered_stocks()}";
	public static final String SP_UPDATE_OLH_LEVEL = "{call sp_update_olh_level(?)}";
	
	public static final String SP_INSERT_ORDER_INFO = "{call sp_insert_orderinfo(?,?,?,?,?,?,?,?)}";
	
	
	public static final String SP_UPDATE_ORDER_INFO = "{call sp_update_orderinfo(?,?)}";
	
	public static final String SP_INSERT_LOGS = "{call sp_insert_stocklevel(?,?,?,?)}";
	
	
	
	//STOP LOSS AND TARGETS
	public static final String SP_UPDATE_STOPLOSS_IN_TRIGGERINFO = "{call sp_update_sl_triggerinfo(?,?) }";

	enum Task_Options {
		START, SELECT_SMARTMETER, SELECT_POSTINGDATA, EXIT
	};

	

	final static String HOST = "localhost";
	
	
	final static String USERNAME = "root";
	final static String PASSWORD = "root";

	

	final static String CLASSNAME = "com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource";

	/*
	 * Hikari propertises
	 */

	final static long IDELTIMEOUT = 100000;
	final static int MAXIMUMPOOLSIZE = 2;
	final static long MAXLIFETIME = 100000;
	final static long CONNECTIONTIMEOUT = 600000;
	
	final static String DATASOURCE_URL = "jdbc:mysql://localhost:3306/dreemer";

	
}