package com.sud.java.core;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



import com.sud.java.core.model.StockInfo;


public class DataBaseHandler {
	

	public static synchronized boolean isStockLevelFound(String symbol) {
		//System.out.println("readStockLevelDataSet in..");
		Connection connection = null ;
		CallableStatement callableStatement = null;
		boolean isStockLevelFound = false;
		ResultSet resultSet = null;
			try {
				//System.out.println("readStockLevelDataSet in.. for "+symbol);
				connection = HikariSingleTon25.getInstance();
				//System.out.println("readStockLevelDataSet in.. connection instance ");
				callableStatement = connection.prepareCall(Constants.SP_SELECT_STOCKLEVEL);
				callableStatement.setString(1, symbol);
				resultSet = callableStatement.executeQuery();
				if (!resultSet.isBeforeFirst()) {
					isStockLevelFound = false;
					//System.out.println("readStockLevelDataSet----Stock Level not found "+symbol);
					//mListener.onCreateStockLevel(symbol, openPrice);
					//new PriceActionLevelGenerator(mSymbol, mOpen).generateTriggerLevel(mListener);
				}else {
					isStockLevelFound = true;
				}
			}catch (SQLException e) {
				System.out.println("Got exception in readStockLevelDataSet "+e.getLocalizedMessage());
			}catch (Exception e) {
				System.out.println("Got exception in readStockLevelDataSet "+e.getLocalizedMessage());
			}finally {
				try {
					if (resultSet != null && !resultSet.isClosed())
						resultSet.close();
					if (callableStatement != null && !callableStatement.isClosed())
						callableStatement.close();
					if (connection != null && !connection.isClosed())
						connection.close();

				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("Exception close connection in readStockLevelDataSet " + e.getLocalizedMessage());

				}
			}	
			return isStockLevelFound;
		}
	
	public static synchronized List<StockInfo> getFilteredStockList() {
		List<StockInfo> mList = new ArrayList<>();
		Connection connection = null ;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
			try {
				//System.out.println("readStockLevelDataSet in.. for "+symbol);
				connection = HikariSingleTon25.getInstance();
				//System.out.println("readStockLevelDataSet in.. connection instance ");
				callableStatement = connection.prepareCall(Constants.SP_SELECT_FILTERED_STOCK);
				//System.out.println("readStockLevelDataSet in.. call procedure ");
				resultSet = callableStatement.executeQuery();
				
				if (!resultSet.isBeforeFirst()) {
					//System.out.println("readStockLevelDataSet----Stock Level not found "+symbol);
					//mListener.onCreateStockLevel(symbol, openPrice);
					//new PriceActionLevelGenerator(mSymbol, mOpen).generateTriggerLevel(mListener);
				}else {
					//System.out.println("readStockLevelDataSet--------Stock Level FOUND "+symbol);
					while(resultSet.next()) {
						StockInfo stockInfo = new StockInfo();
						stockInfo.setDate(resultSet.getString(1));
						stockInfo.setScriptName(resultSet.getString(2));
						stockInfo.setOpen(resultSet.getDouble(3));
						stockInfo.setLtp(resultSet.getDouble(4));
						stockInfo.setBuyAt(resultSet.getDouble(5));
						stockInfo.setBuySl(resultSet.getDouble(6));
						stockInfo.setTarget1(resultSet.getDouble(7));
						stockInfo.setTarget2(resultSet.getDouble(8));
						stockInfo.setTarget3(resultSet.getDouble(9));
						stockInfo.setTarget4(resultSet.getDouble(10));
						stockInfo.setTarget5(resultSet.getDouble(11));
						stockInfo.setTarget6(resultSet.getDouble(12));
						stockInfo.setSellAt(resultSet.getDouble(13));
						stockInfo.setSellSl(resultSet.getDouble(14));
						stockInfo.setSell_target1(resultSet.getDouble(15));
						stockInfo.setSell_target2(resultSet.getDouble(16));
						stockInfo.setSell_target3(resultSet.getDouble(17));
						stockInfo.setSell_target4(resultSet.getDouble(18));
						stockInfo.setSell_target5(resultSet.getDouble(19));
						stockInfo.setSell_target6(resultSet.getDouble(20));
						stockInfo.setIsHighLow(resultSet.getInt(21));
						mList.add(stockInfo);
					}
					
				}
			}catch (SQLException e) {
				System.out.println("Got exception in readStockLevelDataSet "+e.getLocalizedMessage());
			}catch (Exception e) {
				System.out.println("Got exception in readStockLevelDataSet "+e.getLocalizedMessage());
			}finally {
				try {
					if (resultSet != null && !resultSet.isClosed())
						resultSet.close();
					if (callableStatement != null && !callableStatement.isClosed())
						callableStatement.close();
					if (connection != null && !connection.isClosed())
						connection.close();

				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("Exception close connection in readStockLevelDataSet " + e.getLocalizedMessage());

				}
			}
			return mList;
		}
	
	
	public synchronized static List<String> getStockList() {
		List<String> mList = new ArrayList<>();
		Connection connection = null ;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		try {
			connection = HikariSingleTon25.getInstance();
			callableStatement = connection.prepareCall(Constants.SP_SELECT_STOCKLIST);
			resultSet = callableStatement.executeQuery();
			if (!resultSet.isBeforeFirst()) {
				System.out.println("getOrderDetails: Order id not found ");				
			}else {
				while(resultSet.next()) {
					mList.add(resultSet.getString(1));
				}
			}
		}catch (SQLException e) {
			System.out.println("Got exception in getOrderDetails "+e.getLocalizedMessage());
		}catch (Exception e) {
			System.out.println("Got exception in getOrderDetails "+e.getLocalizedMessage());
		}finally {
			try {
				if (resultSet != null && !resultSet.isClosed())
					resultSet.close();
				if (callableStatement != null && !callableStatement.isClosed())
					callableStatement.close();
				if (connection != null && !connection.isClosed())
					connection.close();

			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println("Exception close connection in checkTriggerLevels " + e.getLocalizedMessage());
			}
		}
		return mList;
	}
	
	static private String dbDriver = "jdbc:mysql://www.db4free.net:3306/niftywatcher";

	
	public static synchronized List<StockInfo> getStockInfoList(){
		List<StockInfo> sInfoList = new ArrayList<>();
		try {
			//Class.forName("com.mysql.jdbc.Driver");
			//Class.forName("com.mysql.cj.jdbc.Driver");
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			System.out.println("jdbc:mysql:/www.db4free.net:3306/niftywatcher");

			Connection con =
				       DriverManager.getConnection("jdbc:mysql://db4free.net/niftywatcher?" +
				                                   "user=sudhir&password=sudhirpatil007");
			
			//Connection con = DriverManager.getConnection("jdbc:mysql://db4free.net:3306/niftywatcher;user=root&password=sudhirpatil007"); 
			System.out.println("Connected...");
			//here sonoo is database name, root is username and password  
			Statement stmt=(Statement) con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT created_date,symbol,open,ltp FROM stock_levels");
			//ResultSet rs=stmt.executeQuery("SELECT created_date,symbol,open,ltp FROM stock_levels ORDER BY created_date DESC");
			//ResultSet rs=stmt.executeQuery("SELECT created_date,symbol,open,ltp FROM stock_levels WHERE DATE(created_date)=DATE(NOW()) GROUP BY symbol ORDER BY created_date DESC");  
			while(rs.next()) {
				StockInfo info = new StockInfo();
				info.setDate(rs.getString(1));
				info.setScriptName(rs.getString(2));
				info.setOpen(rs.getDouble(3));
				info.setLtp(rs.getDouble(4));
				sInfoList.add(info);
				System.out.println("got Data..."+info.getScriptName());
				/*JsonObject obj = new JsonObject();
				obj.addProperty("date", rs.getString(1));
				obj.addProperty("symbol", rs.getString(2));
				obj.addProperty("open", rs.getDouble(3));
				obj.addProperty("ltp", rs.getDouble(4));
				array.add(obj);*/
			}
			con.close();  
		}catch(Exception e){ 
			//e.printStackTrace();
			System.out.println(e);
		}
		System.out.println("Closed...");
		return sInfoList;
	}
	
	public static synchronized String getStockInfoJson(){
		StringBuffer sbf = new StringBuffer();
		try {
			/*Class.forName("com.mysql.jdbc.Driver");  
			Connection con=DriverManager.getConnection("jdbc:mysql://db4free.net:3306/niftywatcher","sudhir","sudhirpatil007");  
			//here sonoo is database name, root is username and password  
			Statement stmt=(Statement) con.createStatement();*/  
			//ResultSet rs=stmt.executeQuery("SELECT created_date,symbol,open,ltp,high,low FROM stock_levels WHERE DATE(`created_date`)=DATE(NOW()) GROUP BY symbol ORDER BY created_date DESC limit 10");
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			

			Connection con =
				       DriverManager.getConnection("jdbc:mysql://localhost:3306/niftywatcher?" +
				                                   "user=sudhir&password=smp@007");
			
			//Connection con = DriverManager.getConnection("jdbc:mysql://db4free.net:3306/niftywatcher;user=root&password=sudhirpatil007"); 
			System.out.println("Connected...");
			//here sonoo is database name, root is username and password  
			Statement stmt=(Statement) con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT created_date,symbol,open,ltp,high,low FROM stock_levels");
			sbf.append("{ \"stockinfo\": [");
			//sbf.append("{[");
			String token="";
			while(rs.next()) {
				
				sbf.append(token+"{");
				sbf.append("\"date\": \""+rs.getString(1)+"\",");
				sbf.append("\"symbol\": \""+rs.getString(2)+"\",");
				sbf.append("\"open\": \""+rs.getString(3)+"\",");
				sbf.append("\"ltp\": \""+rs.getString(4)+"\",");
				sbf.append("\"high\": \""+rs.getString(5)+"\",");
				sbf.append("\"low\": \""+rs.getString(6)+"\"");
				System.out.println("Connected..."+rs.getString(2));
				/*JSONObject obj = new JSONObject();
				obj.put("date", rs.getString(1));
				obj.put("symbol", rs.getString(2));
				obj.put("open", rs.getDouble(3));
				obj.put("ltp", rs.getDouble(4));*/
				sbf.append("}");
				token=",";
				//sStockInfo.put(obj);
			}
			sbf.append("]}");
			con.close();
			System.out.println("Closed...");
		}catch(Exception e){ 
			System.out.println(e);
		}
		return sbf.toString();
	}
}
