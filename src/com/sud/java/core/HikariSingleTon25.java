package com.sud.java.core;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariConfig;

public class HikariSingleTon25 {
	private static HikariDataSource ds;
	// private String username, password, host, /* dbname, */classname, url;
	// private int maxPoolSize;
	// private long maxLifeTime, idleTimeout, connectionTimeOut;
	private Properties properties;

	public HikariSingleTon25() {

	}

	public static Connection getInstance() throws SQLException {
		if (ds != null) {
			return ds.getConnection();
		} else {
			HikariSingleTon25 ton = new HikariSingleTon25();
			ds = ton.contextInitialized();
			System.out.println("Data Source Created .25 ");
			return ds.getConnection();
		}
	}

	public static void contextDestroyed() {
		System.out.println("Data Source destroyed .25");
		ds.close();
	}

	public HikariDataSource contextInitialized() {

		properties = new Properties();

		properties.setProperty("dataSource.user", Constants.USERNAME);
		properties.setProperty("dataSource.password", Constants.PASSWORD);
		properties.setProperty("dataSourceClassName", Constants.CLASSNAME);
		properties.setProperty("dataSource.url", Constants.DATASOURCE_URL);

		try {
			final HikariConfig config = new HikariConfig();
			config.setMaximumPoolSize(15);
			config.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
			config.addDataSourceProperty("serverName", "localhost");
			config.addDataSourceProperty("port", "3306");
			config.addDataSourceProperty("databaseName", "newdatabase");
			config.addDataSourceProperty("user", "root");
			config.addDataSourceProperty("password", "Beer");

			HikariConfig hikariconfig = new HikariConfig(properties);
			//hikariconfig.setInitializationFailFast(false);
			hikariconfig.setConnectionTestQuery("/* ping */");
			hikariconfig.setIdleTimeout(Constants.IDELTIMEOUT);
			hikariconfig.setMaxLifetime(Constants.MAXLIFETIME);
			hikariconfig.setConnectionTimeout(Constants.CONNECTIONTIMEOUT);
			// hikariconfig.setMinimumIdle(100);
			hikariconfig.setMaximumPoolSize(Constants.MAXIMUMPOOLSIZE);
			return new HikariDataSource(hikariconfig);
			
		} catch (Exception e) {
			System.out.println("Exception in HikariSingle \t"+ e.getLocalizedMessage());
			e.printStackTrace();
		}
		return null;
	}

}
