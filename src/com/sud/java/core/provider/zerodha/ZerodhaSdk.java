package com.sud.java.core.provider.zerodha;

import com.zerodhatech.kiteconnect.KiteConnect;
import com.zerodhatech.kiteconnect.kitehttp.SessionExpiryHook;
import com.zerodhatech.kiteconnect.kitehttp.exceptions.KiteException;
import com.zerodhatech.models.Margin;
import com.zerodhatech.models.User;


public class ZerodhaSdk {
	
	public static void initKite(String userId,String apiKey,String apiSecret,String requestToken) throws KiteException {
		try {
			// First you should get request_token, public_token using kitconnect login and then use request_token, public_token, api_secret to make any kiteConnect api call.
            // Initialize KiteSdk with your apiKey.
            KiteConnect kiteConnect = new KiteConnect("xxxxxxyyyyyzzz");

            // Set userId
            kiteConnect.setUserId("xxxxyyy");

            //Enable logs for debugging purpose. This will log request and response.
            kiteConnect.setEnableLogging(true);

            // Get login url
            String url = kiteConnect.getLoginURL();

            // Set session expiry callback.
            kiteConnect.setSessionExpiryHook(new SessionExpiryHook() {
                @Override
                public void sessionExpired() {
                    System.out.println("session expired");
                }
            });

            /* The request token can to be obtained after completion of login process. Check out https://kite.trade/docs/connect/v1/#login-flow for more information.
               A request token is valid for only a couple of minutes and can be used only once. An access token is valid for one whole day. Don't call this method for every app run.
               Once an access token is received it should be stored in preferences or database for further usage.
             */
            User user =kiteConnect.generateSession(requestToken, apiSecret);
            kiteConnect.setAccessToken(user.accessToken);
            kiteConnect.setPublicToken(user.publicToken);

            ProfileApi profile = new ProfileApi();

            profile.getProfile(kiteConnect);

            profile.getMargins(kiteConnect);
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

}
